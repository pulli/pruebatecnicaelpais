@extends('layouts.app')
@section('contenido')

      <!-- Top content -->
      <div class="top-content">
          
          <div class="inner-bg">
              <div class="container">
                  <div class="row">
                      <div class="col-sm-8 col-sm-offset-2 text">
                          <h1 style="color:#797D7F";  ><strong  style="color:#090C0C"; >Login</strong><b> App de Actividades</b></h1>
                         
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-sm-6 col-sm-offset-3 form-box " style="#C70039";>
                        <div class="form-top" >
                          <div class="form-top-left">
                            <h2 style="color:#F5F0F1";><b>Inicio de Sesión</b></h2>
                             
                          </div>
                          <div class="form-top-right">
                            <i class="fa fa-lock"></i>
                          </div>
                          </div>
                          <div class="form-bottom">
                             <!--Inicio de Formulario -->
          <form method="POST" id="login-form" role="form" style="display: block;" action="{{route('login')}}" >
                     {{csrf_field() }}
                     <div class="form-group {{$errors->has('email') ? 'has-error': ''}}">
                            <label class="sr-only" for="form-username">Email</label>
                            <input class="form-control" 
                            type="email" 
                            name="email" 
                            id="email"
                            tabindex="1"
                            value="{{old('email')}}"
                            placeholder="Ingresa tu email..." >
                            {!! $errors->first('email', '<span class="help-block" style="color:#FFFFFF";>:message</span>') !!}
                         </div>
                         <div class="form-group {{$errors->has('password') ? 'has-error' : ''}}">
                
                              <label class="sr-only" for="form-password">Contraseña</label>
                              <input class="form-control" 
                              type="password" 
                              name="password" 
                              id="password" 
                              tabindex="2" 
                              placeholder="Ingresa tu contraseña ..." >
                              {!! $errors->first('password', '<span class="help-block" style="color:#FFFFFF";>:message</span>') !!}
                           </div><!-- Fin  for group de Contraseña -->
                           
                            <button type="submit" class="btn">Ingresar
                            </button>
                        </form>

                        <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 ">
                    <!-- -->
                   </div>
                      <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 ">
                        <a name="olvidocontraseña"  style="color:#FFFFFF";>Olvido su contraseña?</a>   
                            <a href=""> Recuperar Contraseña   </a>      
                      </div>
                   </div> 

                   <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 ">
                    <!-- -->
                   </div>
                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 ">
                        <a name="registrarme"  style="color:#FFFFFF";>Usuario nuevo?</a>   
                            <a href="{{route('registrarUsuario')}}"> Registrarme   </a>      
                      </div>
                   </div>
                      </div>

                      </div>
              
                  
                  
                      
                  </div>
                   
              </div>
          </div>
          
      </div>

      @endsection