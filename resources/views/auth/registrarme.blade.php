@extends('layouts.app')
@section('contenido')

 <!-- Top content -->
 <div class="top-content">
        	
          <div class="inner-bg">
              <div class="container">
                
                  <div class="row">
                      <div class="col-sm-6 col-sm-offset-3 form-box">
                        <div class="form-top">
                          <div class="form-top-left">
                            <h2 style="color:#F5F0F1";><b>Registrarme</b></h2>
                             
                          </div>
                          <div class="form-top-right">
                            <i class="fa fa-lock"></i>
                          </div>
                          </div>
                          <div class="form-bottom">
                             <!--Inicio de Formulario -->
                             <form id="register-form" method="POST" action="{{route('registro')}}" role="form" > 
                             {{csrf_field() }}

                          <div class="form-group {{$errors->has('nombreRegistro') ? 'has-error': ''}}">
                             <input type="text" class="form-control" tabindex="1"  id="nombreRegistro"  name="nombreRegistro" value="{{old('nombreRegistro')}}" placeholder="Ingresa tu Nombre..."  >
                             {!! $errors->first('nombreRegistro', '<span id="span1" class="help-block" style="color:#FFFFFF";>:message</span>') !!}
                         </div>
                         <!--fin nombre de usuario -->

                           <div class="form-group {{$errors->has('emailRegistro') ? 'has-error': ''}}">
                             <input type="text" class="form-control" tabindex="1"  id="emailRegistro"  name="emailRegistro" value="{{old('emailRegistro')}}" placeholder="Ingresa tu Email..."  >
                             {!! $errors->first('emailRegistro', '<span id="span1" class="help-block" style="color:#FFFFFF";>:message</span>') !!}
                         </div>
                         <!--fin nombre de email -->

                           <div class="form-group {{$errors->has('passwordRegistro') ? 'has-error': ''}}">
                             <input type="text" class="form-control" tabindex="1"  id="passwordRegistro"  name="passwordRegistro" value="{{old('passwordRegistro')}}" placeholder="Ingresa la Contraseña..."  >
                             {!! $errors->first('passwordRegistro', '<span id="span1" class="help-block" style="color:#FFFFFF";>:message</span>') !!}
                         </div>
                         <!--fin nombre de contraseña -->

                           <div class="form-group {{$errors->has('ConfirpasswordRegistro') ? 'has-error': ''}}">
                             <input type="text" class="form-control" tabindex="1"  id="ConfirpasswordRegistro"  name="ConfirpasswordRegistro" value="{{old('ConfirpasswordRegistro')}}" placeholder="Confirma tu contraseña..."  >
                             {!! $errors->first('ConfirpasswordRegistro', '<span id="span1" class="help-block" style="color:#FFFFFF";>:message</span>') !!}
                         </div>
                         <!--fin nombre de confirmar contraseña -->
                      
                         <div class="form-group">
                         <div class="row">
                           <div class="col-sm-6 col-sm-offset-3"> 
                                     <button   name="register-submit" id="register-submit" tabindex="2" class="form-control btn btn-register" >ENVIAR</button>
                           </div>
                      
                    </div>
                  </div>
              </form>
              <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                        <a href="/">  <button   name="atras" id="atras" tabindex="2" class="form-control btn btn-register" >Ir Login</button></a>
                </div>
            </div>

                      </div>
                     
                      
                  </div>
                   
              </div>
          </div>
          
      </div>

@endsection