@extends('layouts.plantilla')
@section('contenido')
<h3>Index de categoria</h3>

    @include('categoria.modal.modalCrearCategoria') 
     @include('categoria.modal.modalEliminarCategoria') 
    <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
        <a href="" data-target="#ModalCrearCategoria" data-toggle="modal">  <button type="button" class="btn btn-primary" ><span class="fa fa-plus-circle" ></span> Crear Categoria</button>
        </a>
<!-- //////////////////////////////////// -->

 <div class="container">
                    <br />
              <div class="col-md-8">      
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Operaciones</th>
                          
                           <!-- <th>Progreso</th>-->
                           
                        </tr>
                        </thead>
                        <tbody id="datosCategoria">  
                        </tbody>         
                      
                    </table>
               </div> 
                 <div class="col-md-4">     
                 </div> 
         </div>


@stop

@section('scripts')
<script src="../js/scriptCategoria.js"></script>
@endsection