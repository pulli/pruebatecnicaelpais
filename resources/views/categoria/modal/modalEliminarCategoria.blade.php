<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modalElimCategoria">
	
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
			
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true" class="" >×</span>
                </button>
                <h2 class="modal-title">Eliminar Categoria</h2>
			</div>
			<div class="modal-body">
			<input type="hidden" name="_token" value="{{ csrf_token() }}" id="tokenDeleteT">
	
				<h4>Desea Eliminar? </h4>
				<input type="hidden" id="idCategoriaEliminar"> </input>
			</div>
			<div class="modal-footer">
			
			<button  OnClick="eliminarTarea(this);"  type="button" id="eliminarCategoria" class="btn btn-primary">Confirmar
                <span class="fa fa-check-circle"></span> </button>
				<button type="button" id="tareaConsulta" class="btn btn-danger" data-dismiss="modal">Cancelar 
                <span class="fa fa-times-circle"></span> </button>
				
			</div>
		</div>
	</div>
	

</div>