
  <!-- Modal -->
  <div class="modal fade fade modal-slide-in-right" id="ModalCrearCategoria" aria-hidden="true"  tabindex="-1" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Crear Tarea</h4>
        </div>
        <!--  Modal body-->
        <div class="modal-body">
             <div class="row">
             	
				<div class="col-md-12"> 
<input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
						<div class="form-group">
			                {!!Form::label('Nombre:')!!}
			                <br>
			                 <!--       <label for="hu">HU- </label> -->
			                <input  class="form-control" type="text"  name="nombreCategoria" id="nombreCategoria" placeholder="Ingrese el nombre de la cate... " />
			             </div>

			          
			            <div class="form-group">
				            {!! Form::label('Descripcion:') !!}
				            <textarea type="text" onkeypress="return soloLetras(event)" class="form-control" rows="5" name="descripcionCategoria" id="descripcionCategoria" placeholder="Ingrese la descripción de la cate..."></textarea>
			       	    </div>
				</div>
          </div>
        </div>
       <!-- Fin modal body-->
        <div class="modal-footer">
        	 {!!link_to('#',$title='Registrar',$atributtes=['id'=>'registroCategoria','type'=>'submit','class'=>'btn btn-primary'],$secure=null)!!}
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  


