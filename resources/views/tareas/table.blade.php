<table class="table table-hover table-sm">
                        <thead>
                        <tr>
                            <th>Id Tarae</th>
                            <th>Nombre</th>
                            <th>Fecha Inicio</th>
                            <th>Fecha Fin</th>
                            <th>Prioridad</th>
                            <th>Estado</th>
                            <th>Categoria</th>
                           <th>Operaciones</th>
                           
                        </tr>
                        </thead>
                        <tbody >  
                            @foreach($tareas as $tar)
                            <tr>
                                <td>{{$tar->id}}</td>
                                <td>{{$tar->nombre}}</td>
                                <td>{{$tar->fechaInicio}}</td>
                                <td>{{$tar->fechaFin}}</td>
                                <td>{{$tar->prioridad}}</td>
                                <td>{{$tar->estado}}</td>
                                <td>{{$tar->nombreCategoria}}</td>
                                <td>  <a href="" data-target="#ModalActualizarTarea" data-toggle="modal">  <button type="button" class="btn btn-warning" value="{{$tar->id}}" OnClick='actuaTareaModal(this);'  ><span class="fa fa-edit" ></span> Editar</button> </a></td>
                               <td><a href="" data-target="#modalElimTarea" data-toggle="modal">  <button type="button" class="btn btn-danger" value="{{$tar->id}}" OnClick='eliminaTareaModal(this);'  ><span class="fa fa-trash" ></span> Eliminar</button> </a></td>
                            </tr>
                            @endforeach
                        </tbody>         
                      
</table>
                    {!!$tareas->render()!!}


       