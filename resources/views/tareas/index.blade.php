@extends('layouts.plantilla')
@section('contenido')
<h3>Lista de Actividades </h3>


 <div id="msj-successC" class="alert alert-success alert-dismissible" role="alert" style="display:none">
<strong>Actividad Creado Correctamente.</strong>
</div>

<div id="msj-success" class="alert alert-info alert-dismissible" role="alert" style="display:none">
        <strong> Actividad Actualizada Correctamente.</strong>
    </div>

    <div id="msj-danger" class="alert alert-danger alert-dismissible" role="alert" style="display:none">
        <strong> Acticidad Eliminada Correctamente.</strong>
    </div>
    <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
        <a href="" data-target="#ModalCrearTarea" data-toggle="modal">  <button type="button" class="btn btn-primary" ><span class="fa fa-plus-circle" ></span> Crear Tarea</button>
        </a>

      
       
         <div class="input-group">
                    <input type="text" name="scope" id="scope" class="form-control" placeholder="Digite nombre de la Actividad">
            <span class="input-group-btn">
            <a href="#" ><button class="btn btn-primary" id="btnbuscarActi" type="button" ><span class="fa fa-search" style="color:white" ></span></button></a>
            </span>
         </div>



          
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">      
                     <div class="panel-heading">Actividades</div>
                         <div class="table-responsive" id="divTable">
                              @component('tareas.table')
                                @slot('tareas',$tareas)
                              @endcomponent
                         </div>
                </div>
             </div>         
         
  @include('tareas.modal.modalCrearTarea') 
@include('tareas.modal.modalEliminarTarea')
@include('tareas.modal.modalActualizarTarea') 
@endsection

@section('scripts')
<script src="../js/scriptTarea.js"></script>
<script src="../js/scriptAutoCompleteTarea.js"></script>
<script src="../js/jquery-ui.min.js"></script>

@endsection



   