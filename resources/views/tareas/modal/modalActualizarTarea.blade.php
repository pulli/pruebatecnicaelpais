<!-- Modal -->
  <div class="modal fade fade modal-slide-in-right" id="ModalActualizarTarea" aria-hidden="true"  tabindex="-1" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Editar Tarea</h4>
          <input type="hidden" id="idTareaActualizar"> </input>
        </div>
        <!--  Modal body-->
        <div class="modal-body">
        		  <div id="msj-errors" class="alert alert-danger fade in" style="display:none">
				    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				    <span id="msj-errors-text"></span>
				</div>
             <div class="row">
             	
				<div class="col-md-12"> 
					<div class="col-md-6"> 
						    

						<div class="form-group">
			                {!!Form::label('Nombre:')!!}
			                <br>
			                 <!--       <label for="hu">HU- </label> -->
			                <input  class="form-control" type="text"  name="nombreTareaActualizar" id="nombreTareaActualizar" placeholder="Ingrese el nombre de la Tare... " />
			                  
			             </div>

			          
			            <div class="form-group">
				            {!! Form::label('Descripcion:') !!}
				            <textarea type="text"  class="form-control" rows="5" name="descripcionTareaActualizar" id="descripcionTareaActualizar" placeholder="Ingrese la descripción de la tarea..."></textarea>
			       	    </div>

			       	    <div class="form-group">
		                     {!!Form::label('Categoria:')!!}
		                    <select name="CategoriaTareaActualizar" id="CategoriaTareaActualizar" class="form-control" >
							  
							  
		  					</select>
                        </div>

				   </div>		<!--FIN COL-MD-IZQUIERDO -->      

				   <div class="col-md-6">     
						 <div class="form-group">
				            {!! Form::label('Inicio:') !!}
		       <input id="fechaInicioTareaActualizar" type="text" value="" name="fechaInicioTareaActualizar" class="form-control" required min=<?php $hoy=date("Y-m-d"); echo $hoy;?>>
        				</div>

          		  <div class="form-group">
		            {!! Form::label('Fin:') !!}
		            <input id="fechaFinTareaActualizar" type="text" value="" name="fechaFinTareaActualizar" class="form-control" required min=<?php $hoy=date("Y-m-d"); echo $hoy;?>>
       			 </div>

                 <div class="form-group">
                     {!!Form::label('Prioridad:')!!}
                    <select name="prioridadCrearTActualizar" id="prioridadCrearTActualizar" class="form-control" >
					   <option value="" disabled selected>Seleccione una prioridad...</option>
					    <option value="Baja" >Baja</option>
					    <option value="Media">Media</option>
					    <option value="Alta">Alta</option>
  					</select>
                 </div>
                <br>
                  <div class="form-group">
                     {!!Form::label('Estado:')!!}
                    <select name="estadoTareaActualizar" id="estadoTareaActualizar" class="form-control" >
					   <option value="" disabled selected>Seleccione el Estado de la ta...</option>
					    <option value="PorHacer" >Por hacer</option>
					    <option value="Pendiente">Pendiente</option>
					    <option value="Hecha">Hecha</option>
  					</select>
                 </div>

				    </div><!-- FIN COL-MD-6 DERECHA-->
				</div>
          </div>
        </div>
       <!-- Fin modal body-->
        <div class="modal-footer">
        	 {!!link_to('#',$title='Registrar',$atributtes=['id'=>'actualizarTarea','type'=>'submit','class'=>'btn btn-primary'],$secure=null)!!}
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  

