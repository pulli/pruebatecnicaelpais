<?php

namespace ToDoList;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $table='categoria';
    protected $primaryKey='idcategoria';
    public $timestamps=false;

    protected $fillable=[
     'nombreCategoria',
     'descripcionCategoria',
     'Fk_idusuario',
     'condicionCategoria'
    ];

    
}
