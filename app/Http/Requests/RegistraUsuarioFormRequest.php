<?php

namespace ToDoList\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegistraUsuarioFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             'nombreRegistro'=>'required|string',
             'emailRegistro'=>'required|email|string',
             'passwordRegistro'=>'required|string',
             'ConfirpasswordRegistro'=>'required|string', 
        ];
    }

     public function messages()
    {
    return [
    'nombreRegistro.required' => 'El campo Nombre es obligatorio',
    'emailRegistro.required' => 'El campo Email es obligatorio',
    'passwordRegistro.required' => 'El campo Contraseña es obligatorio',
    'ConfirpasswordRegistro.required' => 'El campo Confirmar Contraseña es obligatorio',

    ];
    }


}//fin de la classe
