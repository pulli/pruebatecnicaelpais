<?php

namespace ToDoList\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AutenticacionFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            'email' =>'required|email|string',
            'password' =>'required|string',  
        ];
    }


    public function messages()
    {
    return [
    'email.required' => 'El campo Email es obligatorio',
    'password.required' => 'El campo contraseña es obligatorio',
    ];
    }

}//fin de la clase
