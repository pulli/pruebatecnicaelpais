<?php

namespace ToDoList\Http\Controllers\Auth;
use Illuminate\Support\Facades\Input;
use ToDoList\Http\Controllers\Controller;
use ToDoList\Http\Requests\AutenticacionFormRequest;
use Auth;
class LoginController extends Controller
{
     public function __construct()
      { 
     $this->middleware('guest',['only'=>'showLoginForm']);
      }

    public function showLoginForm()
    {

       return view('auth.login');
    }

   public function login( AutenticacionFormRequest $request)
   {
 
   if(Auth::attempt(['correo'=>$request['email'] , 'password'=>$request['password']]))
      {
        return redirect()->route('inicioUsuario');
      }
        return back()
        ->withErrors(['email'=>'Las Credenciales no concuerdan con nuestros registros'])
        ->withInput(request(['email']));
    } 

    public function logout(){
         Auth::logout();
        return redirect('/');
    }

   public function registrarUsuario(){
      return view('auth.registrarme');
    }



}//fin de la clase
