<?php

namespace ToDoList\Http\Controllers;

use Illuminate\Http\Request;
use ToDoList\Categoria;
use ToDoList\Tarea;
use ToDoList\User;
use Illuminate\Support\Facades\Redirect;
use ToDoList\Http\Request\CategoriaFormRequest;
use DB;

class CategoriaController extends Controller
{
     public function __construct()
      { 
     $this->middleware('auth');
      }

     public function listing()
     {
       $id=auth()->User()->id;
           $categoria= DB::table('users as user')
             ->join ('categoria as cate','cate.Fk_idusuario','=','user.id')
              ->select('cate.*')
              ->where('user.id','=',$id)
              ->get();
            return response()->json($categoria->toArray());
     } 

    public function index()
    {
          return view('categoria.index');
    }

      public function crtate()
    {

    }

      public function store(Request $request)
    {
                 if($request->ajax()){
                     $idUser=auth()->User()->id;
                Categoria::create([
                    'nombreCategoria'=>$request['nombreCategoria'],
                    'descripcionCategoria'=>$request['descripcionCategoria'],
                    'Fk_idusuario'=>$idUser,
                    'condicionCategoria'=>1,
                            ]);
            return response()->json([
              "mensaje"=> "creado"
            ]);
        }
    }

      public function show()
    {

    }

      public function edit()
    {

    }

       public function update()
    {

    }

      public function destroyCate(Request $request)
    {

      $categoria= Categoria::findOrFail($request->get('idCategoriaEliminar')); 
      $categoria->delete();
       return response()->json(["mensaje"=>"CategoriaEliminada"]);
    }


}
