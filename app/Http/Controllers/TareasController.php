<?php

namespace ToDoList\Http\Controllers;

use Illuminate\Http\Request;
use ToDoList\Http\Requests\TareaRequest;
use DB;
use ToDoList\Categoria;
use ToDoList\Tarea;
use ToDoList\User;
use Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
class TareasController extends Controller
{


	 public function __construct()
      { 
     $this->middleware('auth');
      }


     public function listing()//funcion para cargar las categorias en el modal de crear tarea
     {
       
           $id=auth()->User()->id;
           $categoria= DB::table('users as user')
             ->join ('categoria as cate','cate.Fk_idusuario','=','user.id')
              ->select('cate.*')
              ->where('user.id','=',$id)
              ->get();
            return response()->json($categoria->toArray());
     } 

    public function index(Request $request)
    {
    	$id=auth()->User()->id;
    	 /* $tareas= DB::table('users as user')
    	  ->join ('categoria as cate','cate.Fk_idusuario','=','user.id')
             ->join ('tarea as tar','cate.idcategoria','=','tar.Fk_idcategoria') 
              ->select('user.*', 'tar.*','cate.*')
              ->where('user.id','=',$id)
              ->orderBy('user.id','desc')
              ->paginate(2);*/
              //dd($request->scope);
        try {
              if($request->scope!=null){
           
                 $tareas = Categoria::join('users', 'categoria.Fk_idusuario', '=', 'users.id')
                      ->join('tarea','tarea.Fk_idcategoria','=','categoria.idcategoria')
                      ->select('users.*','categoria.*', 'tarea.*')
                      ->where('tarea.nombre','LIKE','%'.$request->scope.'%')
                       ->where('users.id','=',$id)
                       ->orderBy('tarea.id','desc')
                      ->paginate(2);
                  }else{
                     $tareas = Categoria::join('users', 'categoria.Fk_idusuario', '=', 'users.id')
                      ->join('tarea','tarea.Fk_idcategoria','=','categoria.idcategoria')
                      ->select('users.*','categoria.*', 'tarea.*')
                     ->where('users.id','=',$id)
                       ->orderBy('tarea.id','desc')
                      ->paginate(2);
                  } 

             } catch (QueryException $e) {
                return $e;
             }   

              if($request->ajax())
              {
                 return response()->json(view('tareas.table', compact('tareas'))->render());
              }
       return view('tareas.index', compact('tareas'));
    }


      public function store(Request $request)
    {
                 if($request->ajax()){
                Tarea::create([
                    'nombre'=>$request['nombreTarea'],
                    'descripcion'=>$request['descripcionTarea'],
				     'fechaInicio'=>$request['fechaInicioTarea'],
				     'fechaFin'=>$request['fechaFinTarea'],
				     'prioridad'=>$request['prioridadCrearT'],
				     'Fk_idcategoria'=>$request['CategoriaTarea'],
				    
				     'estado'=>$request['estadoTarea'],
				     'condicion'=>1,
                            ]);
            return response()->json([
              "mensaje"=> "TareaCreada"
            ]);
        }
    }


     public function buscarActAutocomplete(Request $request)
    {

       $term = Input::get('term');
      $results = array();
      $queries= DB::table('tarea as tar')
         ->select('tar.nombre', 'tar.prioridad','tar.descripcion')
      ->where('tar.nombre','LIKE','%'.$term.'%')
      ->orWhere('tar.prioridad', 'LIKE', '%'.$term.'%')
         ->get();


          foreach ($queries as $query)
      {
          $results[] = [ 'nombre' => $query->nombre, 'value' => $query->nombre ];
      }
      if($results==null){
        return response()->json([
          "mensaje" => "Sin Resultados"
      ]);
      }else{
        return Response::json($results);
      }
    }
    
       public function destroyT(Request $request)
    {

      $EliminarTar= Tarea::findOrFail($request->get('idTareaEliminar')); 
      $EliminarTar->delete();
       return response()->json(["mensaje"=>"TareaEliminada"]);
    }



        public function edit($id){
                     $tarea = Categoria::join('users', 'categoria.Fk_idusuario', '=', 'users.id')
                      ->join('tarea','tarea.Fk_idcategoria','=','categoria.idcategoria')
                      ->select('users.*','categoria.*', 'tarea.*')
                       ->where('tarea.id','=',$id)
                       ->get();


    /* $tarea = Tarea::join('USER','CONOCIMIENTO.v_conocimiento_usuario_fk', '=', 'USER.v_pege_id')
                      ->select('USER.v_nombre_estudiante','USER.v_pege_id', 'CONOCIMIENTO.*')
                      ->where('CONOCIMIENTO.v_conocimiento_id','=',$id)
                      ->get();*/
 
    return response()->json(
        $tarea->toArray()
    );
}
      public function updateT(Request $request){
        if ($request->ajax()){
        $idtarea=$request->get('idTareaActualizar');
        $actualizarTarea = Tarea::findOrFail($idtarea); 
        $actualizarTarea->nombre=$request->get('nombreTareaActualizar');
        $actualizarTarea->descripcion=$request->get('descripcionTareaActualizar');
        $actualizarTarea->fechaInicio= $request->get('fechaInicioTareaActualizar');
        $actualizarTarea->fechaFin=$request->get('fechaFinTareaActualizar');
        $actualizarTarea->prioridad=$request->get('prioridadCrearTActualizar');
        $actualizarTarea->Fk_idcategoria=$request->get('CategoriaTareaActualizar');
        $actualizarTarea->estado=$request->get('estadoTareaActualizar');
        $actualizarTarea->update();
        return response()->json(["mensaje"=>"TareaActualizada"]);
    }
    }



}//fin de la clase
