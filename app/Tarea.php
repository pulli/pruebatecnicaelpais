<?php

namespace ToDoList;

use Illuminate\Database\Eloquent\Model;

class Tarea extends Model
{
    protected $table='tarea';
    protected $primaryKey='id';
    public $timestamps=false;

    protected $fillable=[
     'nombre',
     'descripcion',
     'fechaInicio',
     'fechaFin',
     'prioridad',
     'Fk_idcategoria',
     'estado',
     'condicion',
    ];

       protected $guarded =[
        
            ];
            
    public function scopeSearch($query,$scope=''){
        return $query->where('nombre','like','%scope%');
    }
}
