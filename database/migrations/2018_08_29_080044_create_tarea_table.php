<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTareaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       
        Schema::create('tarea', function (Blueprint $table) {
            $table->increments('id');
             $table->string('nombre',70);
            $table->string('descripcion',150);
             $table->string('fechaInicio');
            $table->string('fechaFin');
            $table->string('prioridad',70);
             $table->integer('Fk_idcategoria')->unsigned();
          
             $table->string('estado',20)->default('PorHacer');
            $table->boolean('condicion')->default(1);
            $table->timestamps();
  //Relaciones
                
                $table->foreign('Fk_idcategoria')->references('idcategoria')->on('categoria')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tarea');
    }
}
