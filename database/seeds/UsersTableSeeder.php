<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'nombre' => str_random(10),
            'correo' => str_random(10).'@gmail.com',
            'password' => bcrypt('secret'),
        ]);
    }
}
