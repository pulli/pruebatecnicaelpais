$(document).ready(function(){
     LeerCategorias();

});

 function LeerCategorias(){
    var tablaDatos=$("#datosCategoria");
var route="http://127.0.0.1:8000/categoriaShow";
$("#datosCategoria").empty();
$.get(route,function(res){
    $(res).each(function(key,value){
        tablaDatos.append("<tr><td>"+value.idcategoria+"</td><td>"+value.nombreCategoria+
        "</td><td><button value="+value.idcategoria+" OnClick='Mostrar(this);' class='btn btn-warning' data-toggle='modal' data-target='#myModalA'><i class='fa fa-edit'></i> Editar</button><td><button class='btn btn-danger' value="+value.idcategoria+" OnClick='eliminaCategoriaModal(this);' data-toggle='modal' data-target='#modalElimCategoria'><i class='fa fa-trash'></i>Eliminar</button></td></tr>'");
  
    });
});
}


$("#registroCategoria").click(function(){

    var nombreCategoria=$("#nombreCategoria").val();
    var descripcionCategoria=$("#descripcionCategoria").val();
    var route="http://127.0.0.1:8000/categoria";
    var token=$("#token").val();
    $.ajax({
    url: route,
    headers:{'X-CSRF-TOKEN': token},
    type: 'POST',
    dataType:'json',
    data:{nombreCategoria:nombreCategoria,descripcionCategoria:descripcionCategoria},
    success:function(query){
        if(query.mensaje=="creado"){ 
            
            $("#ModalCrearCategoria").modal('toggle');
            $("#msj-successC").fadeIn();
            $("#msj-successC").fadeOut(3000);
            document.getElementById('nombreCategoria').value = '';
        document.getElementById('descripcionCategoria').value = '';
        }
      
        },
        error:function(msj){

        	//alert('error al guadar');
            var errormessages="";
            $.each(msj.responseJSON,function(i,field){
errormessages+="<li>"+field+"</li>";
            });
            $("#msj-errors-text").html(
                "<ul>"+
            errormessages+
            "</ul>"
                );
            $("#msj-errors").fadeIn();       
    }      
    });  
    });

function actualizarVistaCategoria(){
    
    $.ajax({
        type: 'GET',
        url: "/categoriaShow",
        success: function(respuesta) {
            
            
            $('#datosCategoria').html(respuesta);
       }
    });
    
}

setInterval( function(){
    
   // actualizarVistaCategoria();
    
},1000)//Actualizamos cada 1 segundo


function eliminaCategoriaModal(btn){
    $("#idCategoriaEliminar").val(btn.value);  
}

$("#eliminarCategoria").click(function(){
    var idCategoriaEliminar = $("#idCategoriaEliminar").val();
    var route="http://127.0.0.1:8000/categoriaDelete";
    var token=$("#token").val();
    $.ajax({
        url:route,
        headers:{'X-CSRF-TOKEN': token},
        type:'POST',
        dataType:'json',
        data: {idCategoriaEliminar:idCategoriaEliminar},
        success: function(){
            $("#msj-danger").fadeIn();
            $("#msj-danger").fadeOut(3000);
           actualizarVistaTarea();
        }
    });
     $("#modalElimCategoria").modal('toggle');
    }); 