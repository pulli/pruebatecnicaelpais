$(document).ready(function(){
    Carga();

$("#fechaFinTarea").datepicker({ 
    constrainInput: true,   // prevent letters in the input field
    minDate: new Date(),    // prevent selection of date older than today
    autoSize: true,         // automatically resize the input field
    dateFormat: 'dd-mm-yy',  // Date Format used
   // beforeShowDay: $.datepicker.noWeekends,     // Disable selection of weekends
    firstDay: 1 ,// Start with Monday
    currentText: 'Hoy',
monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá']
  });
  $("#fechaInicioTarea").datepicker({
    constrainInput: true,   // prevent letters in the input field
    minDate: new Date(),    // prevent selection of date older than today
    autoSize: true,         // automatically resize the input field
    dateFormat: 'dd-mm-yy',  // Date Format used
  //  beforeShowDay: $.datepicker.noWeekends,     // Disable selection of weekends
    firstDay: 1 ,// Start with Monday
    currentText: 'Hoy',
monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá']
})
	function marcar(source) 
	{
		checkboxes=document.getElementsByTagName('input'); //obtenemos todos los controles del tipo Input
		for(i=0;i<checkboxes.length;i++) //recoremos todos los controles
		{
			if(checkboxes[i].type == "checkbox") //solo si es un checkbox entramos
			{
				checkboxes[i].checked=source.checked; //si es un checkbox le damos el valor del checkbox que lo llamó (Marcar/Desmarcar Todos)
			}
		}
    }
    function soloNumerosH(e){
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key).toLowerCase();
        letras = "123456789";
        especiales = "8-37-39-46";
 
        tecla_especial = false
        for(var i in especiales){
             if(key == especiales[i]){
                 tecla_especial = true;
                 break;
             }
         }
         if(letras.indexOf(tecla)==-1 && !tecla_especial){
             return false;
         }
     }
});
                    //crear TAREA----------------------------------------------------------
$("#registroTarea").click(function(){
   var nombreTarea=$("#nombreTarea").val();
    var descripcionTarea=$("#descripcionTarea").val();
    var CategoriaTarea=$("#CategoriaTarea").val();
    var fechaInicioTarea=$("#fechaInicioTarea").val();
    var fechaFinTarea=$("#fechaFinTarea").val();
    var prioridadCrearT=$("#prioridadCrearT").val();
    var estadoTarea=$("#estadoTarea").val();
  
    var route="http://127.0.0.1:8000/tarea";
    var token=$("#token").val();
    $.ajax({
    url: route,
    headers:{'X-CSRF-TOKEN': token},
    type: 'POST',
    dataType:'json',
    data:{nombreTarea:nombreTarea,descripcionTarea:descripcionTarea,CategoriaTarea:CategoriaTarea,fechaInicioTarea:fechaInicioTarea,fechaFinTarea:fechaFinTarea,prioridadCrearT:prioridadCrearT,estadoTarea:estadoTarea},
    success:function(query){
        if(query.mensaje=="TareaCreada"){ 
            
            $("#ModalCrearTarea").modal('toggle');
            $("#msj-successC").fadeIn();
            $("#msj-successC").fadeOut(3000);
            document.getElementById('nombreTarea').value = '';
            document.getElementById('descripcionTarea').value = '';
            document.getElementById('CategoriaTarea').value = '';
            document.getElementById('fechaInicioTarea').value = '';
            document.getElementById('fechaFinTarea').value = '';
            document.getElementById('prioridadCrearT').value = '';
            document.getElementById('estadoTarea').value = '';
            actualizarVistaTarea();
        }
      
        },
        error:function(msj){

            //alert('error al guadar');
            var errormessages="";
            $.each(msj.responseJSON,function(i,field){
               errormessages+="<li>"+field+"</li>";
            });
            $("#msj-errors-text").html("<ul>"+errormessages+"</ul>");
            $("#msj-errors").fadeIn();       
    }      
    });  
    });

///cargar las categorias en el select del modal crear categoria

function Carga(){
    var tablaDatos=$("#tareas");
 var categorias=$("#CategoriaTarea");
var route="http://127.0.0.1:8000/tareaShow";
tablaDatos.empty();
categorias.empty();
$.get(route,function(res){
     categorias.html("<option id='0' value='0' selected>Seleccione un Categoria.....</option>"); 
    $(res).each(function(key,value){
        //tablaDatos.append("<tr><td>"+value.v_proyecto_id+"</td><td>"+value.v_proyecto_nombre+"</td><td><button value="+value.v_proyecto_id+" OnClick='Mostrar(this);' class='btn btn-warning' data-toggle='modal' data-target='#myModalA'><i class='fa fa-edit'></i> Editar</button><button class='btn btn-danger' value="+value.v_proyecto_id+" OnClick='Eliminar(this);' data-toggle='modal' data-target='#myModalE'><i class='fa fa-trash'></i>Eliminar</button><button value="+value.v_proyecto_id+" OnClick='DesasignarE(this);' class='btn btn-primary' data-toggle='modal' data-target='#myModalDesasignarE'><i class='fa fa-user-times'></i> Desasignar Estudiante</button></td></tr>'");
  categorias.append("<option value='"+value.idcategoria+"'>"+value.nombreCategoria+"</option>")
    });
});
}

//paginacion con jquery

$(document).on('click', '.pagination a', function(e){
    e.preventDefault();//lo uso para bloquear la accion de refrescar toda la pagina
    
    $.ajax({
        url:"/tarea",
        data:{page:$(this).attr('href').split('page=')[1],scope:$("#scope").val()},
        type:'GET',
        dataType:'json',
        success:function(result){
        $("#divTable").html(result);
        },
        error:function(result){
            alert(result.status+':'+result.statusText);
        }

    });
});

$(document).on('click', '#btnbuscarActi ', function(e){
    $.ajax({
        url:"/tarea",
        data:{scope:$("#scope").val()},
        type:'GET',
        dataType:'json',
        success:function(result){
        $("#divTable").html(result);
        },
        error:function(result){
            alert(result.status+':'+result.statusText);
        }
    });
});


//

function eliminaTareaModal(btn){
    $("#idTareaEliminar").val(btn.value);  
}

$("#eliminarTarea").click(function(){
    var idTareaEliminar = $("#idTareaEliminar").val();
    var route="http://127.0.0.1:8000/tareaDelete";
    var token=$("#token").val();
    $.ajax({
        url:route,
        headers:{'X-CSRF-TOKEN': token},
        type:'POST',
        dataType:'json',
        data: {idTareaEliminar:idTareaEliminar},
        success: function(){
            $("#msj-danger").fadeIn();
            $("#msj-danger").fadeOut(3000);
           actualizarVistaTarea();
        }
    });
     $("#modalElimTarea").modal('toggle');
    }); 


//ACTUALIZAR TAREA
        function actuaTareaModal(btn){ 
            $("#idTareaActualizar").val(btn.value);  
            var route="http://127.0.0.1:8000/tarea/"+btn.value+"/edit";
            $.get(route,function(res){
                for(i=0;i<res.length;i++){  
                    CargaCategoriaActuTarea();
        $("#nombreTareaActualizar").val(res[i].nombre);
        $("#descripcionTareaActualizar").val(res[i].descripcion);
        //$("#CategoriaTareaActualizar").val(res[i].nombreCategoria);
        $("#fechaInicioTareaActualizar").val(res[i].fechaInicio);
        $("#fechaFinTareaActualizar").val(res[i].fechaFin);
        $("#prioridadCrearTActualizar").val(res[i].prioridad);
        $("#estadoTareaActualizar").val(res[i].estado);
               }
            });
            }

            function CargaCategoriaActuTarea(){
 var categorias=$("#CategoriaTareaActualizar");
var route="http://127.0.0.1:8000/tareaShow";
categorias.empty();
$.get(route,function(res){
    
    $(res).each(function(key,value){
        //tablaDatos.append("<tr><td>"+value.v_proyecto_id+"</td><td>"+value.v_proyecto_nombre+"</td><td><button value="+value.v_proyecto_id+" OnClick='Mostrar(this);' class='btn btn-warning' data-toggle='modal' data-target='#myModalA'><i class='fa fa-edit'></i> Editar</button><button class='btn btn-danger' value="+value.v_proyecto_id+" OnClick='Eliminar(this);' data-toggle='modal' data-target='#myModalE'><i class='fa fa-trash'></i>Eliminar</button><button value="+value.v_proyecto_id+" OnClick='DesasignarE(this);' class='btn btn-primary' data-toggle='modal' data-target='#myModalDesasignarE'><i class='fa fa-user-times'></i> Desasignar Estudiante</button></td></tr>'");
  categorias.append("<option value='"+value.idcategoria+"'>"+value.nombreCategoria+"</option>")
    });
});
}

//ACTUALIZAR TAREA----------------------------------------------------------
$("#actualizarTarea").click(function(){
    var idTareaActualizar=$("#idTareaActualizar").val();     
   var nombreTareaActualizar=$("#nombreTareaActualizar").val();
    var descripcionTareaActualizar=$("#descripcionTareaActualizar").val();
    var CategoriaTareaActualizar=$("#CategoriaTareaActualizar").val();
    var fechaInicioTareaActualizar=$("#fechaInicioTareaActualizar").val();
    var fechaFinTareaActualizar=$("#fechaFinTareaActualizar").val();
    var prioridadCrearTActualizar=$("#prioridadCrearTActualizar").val();
    var estadoTareaActualizar=$("#estadoTareaActualizar").val();
  
    var route="http://127.0.0.1:8000/tareaActualizar";
    var token=$("#token").val();
    $.ajax({
    url: route,
    headers:{'X-CSRF-TOKEN': token},
    type: 'POST',
    dataType:'json',
    data:{idTareaActualizar:idTareaActualizar,nombreTareaActualizar:nombreTareaActualizar,descripcionTareaActualizar:descripcionTareaActualizar,CategoriaTareaActualizar:CategoriaTareaActualizar,fechaInicioTareaActualizar:fechaInicioTareaActualizar,fechaFinTareaActualizar:fechaFinTareaActualizar,prioridadCrearTActualizar:prioridadCrearTActualizar,estadoTareaActualizar:estadoTareaActualizar},
    success:function(query){
        if(query.mensaje=="TareaActualizada"){ 
           
            $("#ModalActualizarTarea").modal('toggle');
            $("#msj-success").fadeIn();
            $("#msj-success").fadeOut(3000);
            document.getElementById('nombreTareaActualizar').value = '';
            document.getElementById('descripcionTareaActualizar').value = '';
            document.getElementById('CategoriaTareaActualizar').value = '';
            document.getElementById('fechaInicioTareaActualizar').value = '';
            document.getElementById('fechaFinTareaActualizar').value = '';
            document.getElementById('prioridadCrearTActualizar').value = '';
            document.getElementById('estadoTareaActualizar').value = '';
             actualizarVistaTarea();
        }
      
        },
        error:function(msj){
            //alert('error al ACTUALIZAR');
        var errormessages="";
            $.each(msj.responseJSON,function(i,field){
               errormessages+="<li>"+field+"</li>";
            });
            $("#msj-errors-text").html("<ul>"+errormessages+"</ul>");
            $("#msj-errors").fadeIn();      
    }      
    });  
    });

function actualizarVistaTarea(){
    
    $.ajax({
        type: 'GET',
        url: '/tarea',
        success: function(respuesta) {
            
            
            $('#divTable').html(respuesta);
       }
    });
    
}

setInterval( function(){
    
   // actualizarVistaTarea();
    
},1000)//Actualizamos cada 1 segundo