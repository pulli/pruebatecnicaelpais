<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('auth.login');
});*/
Route::get('/', 'Auth\LoginController@showLoginForm')->name('/');

Route::resource('categoria','CategoriaController');
Route::resource('tarea','TareasController');
Route::get('categoriaShow','CategoriaController@listing');
Route::get('tareaShow','TareasController@listing');
Route::post('login','Auth\LoginController@login')->name('login');
Route::post('logout','Auth\LoginController@logout');; //ruta del logout
Route::get('inicioUsuario','TareasController@index')->name('inicioUsuario');
Route::get('registrarUsuario','Auth\LoginController@registrarUsuario')->name('registrarUsuario');
Route::post('registro','RegistrarUsuarioController@store')->name('registro');
Auth::routes();
Route::get('buscar','TareasController@buscarActAutocomplete')->name('buscar');

Route::post('tareaDelete','TareasController@destroyT')->name('tareaDelete');
Route::post('categoriaDelete','CategoriaController@destroyCate')->name('categoriaDelete');
Route::post('tareaActualizar','TareasController@updateT');
//Route::post('tareaEdit','ConocimientoController@updateC');
Route::get('/home', 'HomeController@index')->name('home');
